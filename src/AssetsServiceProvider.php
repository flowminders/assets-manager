<?php

namespace FlowControl\Assets;

use Illuminate\Support\ServiceProvider;

class AssetsServiceProvider extends ServiceProvider
{
    /**
     * Register the service provider. 1,2,3
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(Manager::class, function(){
            return new Manager($this->app['session']);
        });

        $this->app->alias(Manager::class, 'flowcontrol.assets');
    }
}