<?php

namespace FlowControl\Assets;


use Illuminate\Support\Facades\Facade;

class AssetsFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'flowcontrol.assets';
    }
}