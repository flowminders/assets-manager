<?php

namespace FlowControl\Assets\Contracts;

interface Shortcut
{
    public function execute();
}